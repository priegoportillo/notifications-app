import {BehaviorSubject} from 'rxjs'

export const loading$ = new BehaviorSubject(false);
