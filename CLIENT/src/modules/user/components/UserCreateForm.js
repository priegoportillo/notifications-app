import React, { useState } from 'react';
import { useForm } from "react-hook-form";

import { Button } from '@mui/material';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import FormControl from '@mui/material/FormControl';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import { createUser } from '../../../core/api/user.service'

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export const UserCreateForm = ({ reload }) => {
    const [open, setOpen] = useState(false);
    const { control, handleSubmit, register,reset, formState: { errors } } = useForm();
    const onSubmit = values => {
        createUser(values);
        reload();
        handleClose();
    };

    const handleOpen = () =>{ setOpen(true);reset();};
    const handleClose = () => setOpen(false);

    return (
        <div>
            <Button style={{ marginBottom: '10px', float: 'right' }} onClick={handleOpen} variant='contained' color='primary'>Add</Button>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" style={{ textAlign: 'center', marginBottom: '10px' }} variant="h6" component="h2">
                        Add new User
                    </Typography>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <FormControl fullWidth>
                            <TextField
                                {...register("firstName", {
                                    required: "Required",
                                })}
                                style={{ marginBottom: '10px' }} id="firstName" label="firstName" variant="outlined" />
                            <TextField
                                {...register("lastName", {
                                    required: "Required",
                                })}
                                style={{ marginBottom: '10px' }} id="lastName" label="lastName" variant="outlined" />
                            <TextField
                                {...register("email", {
                                    required: "Required",
                                })}
                                style={{ marginBottom: '10px' }} id="email" label="email" variant="outlined" />
                            <TextField
                                {...register("telephone", {
                                    required: "Required",
                                })}
                                style={{ marginBottom: '10px' }} id="telephone" label="telephone" variant="outlined" />
                            <div style={{ marginTop: '10px', float: 'right' }}>
                                <Button style={{ marginRight: '10px' }} type='submit' variant='contained' color='primary'>Create</Button>
                                <Button onClick={handleClose} variant='contained' color='error'>Cancel</Button>
                            </div>
                        </FormControl>
                    </form>
                </Box>
            </Modal>
        </div>
    )
}