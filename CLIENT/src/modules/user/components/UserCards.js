import React, { useEffect, useState } from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
import Grid from '@mui/material/Grid';
import {UserLogTable} from './UserLogTable'
import { getUsers } from '../../../core/api/user.service';
import { getAllCategories } from '../../../core/api/categoy.service';
import { getAllNotificationTypes } from '../../../core/api/notification-type.service';
import { subscribeChannel, unSubscribeChannel } from '../../../core/api/channel.service';
import { subscribeCategory, unSubscribeCategory } from '../../../core/api/subscription.service';
export const UserCards = () => {
    const [userRecords, setUserRecords] = useState();
    const [categories, setCategories] = useState();
    const [notificationTypes, setNotificationTypes] = useState();
    const reset = () => {
        window.location.reload(false);
    }
    const getData = async () => {
        let users = await getUsers();
        setUserRecords(users);
    }
    const getCategories = async () => {
        let categoies = await getAllCategories();
        setCategories(categoies);
    }
    const getNotificationTypes = async () => {
        let notificationTypes = await getAllNotificationTypes();
        setNotificationTypes(notificationTypes);
    }
    const capitalize = (text) => {
        return text[0].toUpperCase() + text.slice(1);
    }
    const checkTag = (array, string) => {
        return array.indexOf(string) > -1 ? true : false;
    }

    const subscribeToChannel = (userId,channel,subscriptions) => {
        const isSubscribable = !checkTag(subscriptions,channel);
        const currentChannel = notificationTypes.find((notificationType)=>{
            return notificationType.name === channel
        });
        if(isSubscribable){
            subscribeChannel(userId,[currentChannel.id]);
        }
        else{
            unSubscribeChannel(userId,[currentChannel.id]);
        }
        reset();
    }
    const subscribeToCategory = (userId,subscription,subscriptions) => {
        const isSubscribable = !checkTag(subscriptions,subscription);
        const currentSubscription = categories.find((category)=>{
            return category.name === subscription
        });
        if(isSubscribable){
            subscribeCategory(userId,[currentSubscription.id]);
        }
        else{
            unSubscribeCategory(userId,[currentSubscription.id]);
        }
        reset();
    }
    useEffect(() => {
        getData();
        getCategories();
        getNotificationTypes();

    }, [])

    return (
        <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justify="center"
            style={{ minHeight: '100vh' }}
        >

            <Grid item xs={3}>
                {
                    userRecords && userRecords.map((user) => {
                        return (
                            <Card key={user.id} style={{ marginBottom: '20px' }}>
                                <CardHeader
                                    avatar={
                                        <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                                            {Array.from(user.fullName)[0].toUpperCase()}
                                        </Avatar>
                                    }
                                    action={
                                        <IconButton aria-label="settings" disabled>
                                            <MoreVertIcon />
                                        </IconButton>
                                    }
                                    title={capitalize(user.fullName)}
                                    subheader={user.email}
                                />
                                <CardContent>
                                    <Typography variant="body2" color="text.secondary">
                                        <p><strong>Telephone:</strong><span>{user.telephone}</span></p>
                                        <Grid container>
                                            <Grid item xs={4}>
                                                <strong >Subscriptions:</strong>
                                            </Grid>
                                            <Grid item xs={4}>
                                                <Stack direction="row" spacing={1} style={{ marginBottom: '10px' }}>
                                                    <Chip onClick={()=>{subscribeToCategory(user.id,"Sports",user.subscriptions)}} style={{cursor:'pointer'}} label="Sports" color="warning" variant={checkTag(user.subscriptions, "Sports") ? "" : "outlined"} />
                                                    <Chip onClick={()=>{subscribeToCategory(user.id,"Movies",user.subscriptions)}} style={{cursor:'pointer'}} label="Movies" color="primary" variant={checkTag(user.subscriptions, "Movies") ? "" : "outlined"} />
                                                    <Chip onClick={()=>{subscribeToCategory(user.id,"Finance",user.subscriptions)}} style={{cursor:'pointer'}} label="Finance" color="success" variant={checkTag(user.subscriptions, "Finance") ? "" : "outlined"} />
                                                </Stack>
                                            </Grid>
                                        </Grid>
                                        <Grid container>
                                            <Grid item xs={4}>
                                                <strong >Channel:</strong>
                                            </Grid>
                                            <Grid item xs={4}>
                                                <Stack direction="row" spacing={1} style={{ marginBottom: '10px' }}>
                                                    <Chip onClick={()=>{subscribeToChannel(user.id,"Push Notification",user.channels)}} style={{cursor:'pointer'}}  label=" On Push" color="warning" variant={checkTag(user.channels, "Push Notification") ? "" : "outlined"} />
                                                    <Chip onClick={()=>{subscribeToChannel(user.id,"E-Mail",user.channels)}} style={{cursor:'pointer'}}  label="E-Mail" color="primary" variant={checkTag(user.channels, "E-Mail") ? "" : "outlined"} />
                                                    <Chip onClick={()=>{subscribeToChannel(user.id,"SMS",user.channels)}} style={{cursor:'pointer'}}  label="SMS" color="success" variant={checkTag(user.channels, "SMS") ? "" : "outlined"} />
                                                </Stack>
                                            </Grid>
                                        </Grid>

                                    </Typography>
                                </CardContent>
                                <CardActions disableSpacing style={{
                                    alignSelf: "stretch",
                                    display: "flex",
                                    justifyContent: "flex-end",
                                    alignItems: "flex-start",
                                }}>
                                    <UserLogTable userId={user.id} fullName={user.fullName}></UserLogTable>
                                </CardActions>
                            </Card>
                        )
                    })
                }
            </Grid>
        </Grid>

    );
}