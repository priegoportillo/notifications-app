import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { Button } from '@mui/material';
import {getNotificationsByUserId} from '../../../core/api/notification.service'
function createData(name, calories, fat, carbs, protein) {
    return { name, calories, fat, carbs, protein };
}

const rows = [
    createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
    createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
    createData('Eclair', 262, 16.0, 24, 6.0),
    createData('Cupcake', 305, 3.7, 67, 4.3),
    createData('Gingerbread', 356, 16.0, 49, 3.9),
];

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    height: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export const UserLogTable = ({userId,fullName}) => {
    const [open, setOpen] = React.useState(false);
    const [notifications, setNotifications] = React.useState([]);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const getData = async () => {
        let users = await getNotificationsByUserId(userId);
        setNotifications(users);
    }

    React.useEffect(()=>{
        getData();
    },[userId]);

    return (
        <div>
            <IconButton aria-label="share" onClick={handleOpen}>
                <ManageSearchIcon />
            </IconButton>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" style={{ textAlign: 'center', marginBottom: '10px' }} variant="h6" component="h2">
                         Notifications Log ({fullName})
                    </Typography>
                    {notifications && <TableContainer component={Paper} style={{ maxHeight: 300 }}>
                        <Table sx={{ minWidth: 650 }} stickyHeader aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Log Date</TableCell>
                                    <TableCell align="center">Subscription</TableCell>
                                    <TableCell align="center">Channel</TableCell>
                                    <TableCell align="center">Content</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {notifications.map((row) => (
                                    <TableRow
                                        key={row.name}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell component="th" scope="row">
                                            {row.logDate}
                                        </TableCell>
                                        <TableCell align="left">{row.subscription}</TableCell>
                                        <TableCell align="left">{row.channel}</TableCell>
                                        <TableCell align="left">{row.content}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    }
                    <Box textAlign='center' variant='contained'>
                        <Button style={{ marginTop: '10px', marginLeft: 'auto', marginRight: 'auto' }} onClick={handleClose} variant='contained' color='error'>Cancel</Button>
                    </Box>
                </Box>
            </Modal>
        </div>
    );
}