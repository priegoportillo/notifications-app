import React, {useState} from 'react';

import Grid from '@mui/material/Grid';

import {UserCards} from '../components/UserCards'
import {UserCreateForm} from '../components/UserCreateForm'


export const UserView = ({}) => {
    const [reloadKey, setReloadKey] = useState(new Date())
    return (
        <div style={{marginTop:'30px'}}>
            <Grid container columns={{ xs: 4, md: 12 }}>
                <Grid item md={12} xs={4}>
                    <UserCreateForm reload={ e=> setReloadKey(new Date())}></UserCreateForm>
                </Grid>
                <Grid item md={12} xs={4}>
                    <UserCards key={reloadKey}></UserCards>
                </Grid>
            </Grid>
        </div>
    )
}