import React, {useState} from 'react';
import {MessageCreateForm} from '../components/MessageCreateForm';
import {MessageCards} from '../components/MessageCards'
import Grid from '@mui/material/Grid';



export const MessageView = ({}) => {
    const [reloadKey, setReloadKey] = useState(new Date())
    return (
        <div style={{marginTop:'30px'}}>
            <Grid container columns={{ xs: 4, md: 12 }}>
                <Grid item md={12} xs={4}>
                    <MessageCreateForm reload={ e=> setReloadKey(new Date())}></MessageCreateForm>
                </Grid>
                <Grid item md={12} xs={4}>
                    <MessageCards key={reloadKey}></MessageCards>
                </Grid>
            </Grid>
        </div>
    )
}