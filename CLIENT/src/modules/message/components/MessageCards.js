import React, { useEffect, useState } from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import { getMessages } from '../../../core/api/message.service'
export const MessageCards = () => {
    const [messageRecords, setMessageRecords] = useState();

    const getData = async () => {
        let messages = await getMessages();
        setMessageRecords(messages);
    }
    useEffect(() => {
        getData();

    }, [])

    return (
        <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justify="center"
            style={{ minHeight: '100vh' }}
        >

            <Grid item xs={3}>
                {
                    messageRecords && messageRecords.map((message) => {
                        return (
                            <Card key={message.id} style={{ marginBottom: '20px' }}>
                                <CardContent>
                                    <Typography variant="h5" component="div">
                                        {message.category}
                                    </Typography>
                                    <Typography variant="body2">
                                        {message.content}
                                    </Typography>
                                </CardContent>
                            </Card>
                        )
                    })
                }
            </Grid>
        </Grid>

    );
}