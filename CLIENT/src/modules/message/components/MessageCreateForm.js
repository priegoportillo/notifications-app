import React, { useState } from 'react';
import { useForm } from "react-hook-form";

import { Button } from '@mui/material';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import FormControl from '@mui/material/FormControl';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import { createMessage } from '../../../core/api/message.service'
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import { getAllCategories } from '../../../core/api/categoy.service'
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export const MessageCreateForm = ({ reload }) => {
    const [open, setOpen] = useState(false);
    const [count, setCount] = React.useState(0);
    const { control, handleSubmit, reset, register, formState: { errors } } = useForm();
    const [currentTypePermissionId, setCurrentTypePermissionId] = useState(-1);
    const [Typepermissions, setTypePermissions] = useState([]);
    const onSubmit = values => {
        createMessage(values);
        reload();
        handleClose();
    };

    const handleOpen = () => { setOpen(true); reset(); setCount(0); };
    const handleClose = () => setOpen(false);
    const handleTypePermissionChange = (event) => {
        setCurrentTypePermissionId(event.target.value);
    };

    const getData = async () => {
        let typePermissions = await getAllCategories();
        setTypePermissions(typePermissions);
    }

    React.useEffect(() => {
        getData();
    }, [])

    return (
        <div>
            <Button style={{ marginBottom: '10px', float: 'right' }} onClick={handleOpen} variant='contained' color='primary'>Add</Button>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" style={{ textAlign: 'center', marginBottom: '10px' }} variant="h6" component="h2">
                        Add new Message
                    </Typography>
                    <form id='messageForm' onSubmit={handleSubmit(onSubmit)}>
                        <FormControl fullWidth>
                            <TextField
                                {...register("content", {
                                    required: "Required",
                                    maxLength: 255,
                                    minLength: 1,
                                })}
                                helperText= {'Remaining characters '+ (255 - count)}
                                onChange={e => setCount(e.target.value.length)}
                                style={{ marginBottom: '10px' }} id="content" label="content" variant="outlined" />
                            <Select
                                style={{ marginBottom: '10px' }}
                                {...register("categoryId", {
                                    required: "Required",
                                })}
                                id="categoryId"
                                value={currentTypePermissionId}
                                onChange={handleTypePermissionChange}
                            >
                                <MenuItem value={-1}>-- Select an option --</MenuItem>
                                {
                                    Typepermissions.map((typePermission) => {
                                        return (<MenuItem key={typePermission.id} value={typePermission.id}>{typePermission.name}</MenuItem>)
                                    })
                                }
                            </Select>
                            <div style={{ marginTop: '10px', float: 'right' }}>
                                <Button style={{ marginRight: '10px' }} type='submit' variant='contained' color='primary'>Create</Button>
                                <Button onClick={handleClose} variant='contained' color='error'>Cancel</Button>
                            </div>
                        </FormControl>
                    </form>
                </Box>
            </Modal>
        </div>
    )
}