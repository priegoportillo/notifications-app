import * as React from 'react';
import { ResponsiveAppBar } from './shared/components/appBar'
import { UserView } from './modules/user/pages/UserView'
import { MessageView } from './modules/message/pages/MessageView'
import { ThemeProvider } from '@mui/material/styles';

import { theme } from './shared/utils/theme';


function App() {
  const [page, setPage] = React.useState('USERS');
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <ResponsiveAppBar onChangeView={(page) => { setPage(page) }}></ResponsiveAppBar>
        {page === 'USERS' && <UserView key={new Date()} />}
        {page === 'MESSAGES' && <MessageView key={new Date()} />}
      </ThemeProvider>
    </div>
  )
}

export default App;
