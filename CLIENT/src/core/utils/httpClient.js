import axios from 'axios'
import {loading$} from '../../shared/utils/loading.observable'
export const get = async (url) => {
    loading$.next(true);
    const response = await axios.get(url);
    loading$.next(false);
    if(response.status == 200)
        return response.data;
    return new Error(response.statusText);
}

export const post = async (url, body) =>{
    loading$.next(true);
    const response = await axios.post(url,body);
    loading$.next(false);
    if(response.status = 200)
        return response.data;
    return new Error(response.statusText);
}

export const put = async (url, body) =>{
    const response = await axios.put(url,body);
    if(response.status = 200)
        return response.data;
    return new Error(response.statusText);
}
