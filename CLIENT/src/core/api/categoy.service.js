import {get} from '../utils/httpClient'
import {baseUrl} from '../../environment/environment'

const api = 'category'

export const getAllCategories = async  () => {
    const url = `${baseUrl}/${api}`;
    const result = await get(url);
    
    return result;
}