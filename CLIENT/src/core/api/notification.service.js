import {get, post} from '../utils/httpClient'
import {baseUrl} from '../../environment/environment'

const api = 'notification'
export const getNotificationsByUserId = async  (userId) => {
    const url = `${baseUrl}/${api}/user/${userId}`;
    const result = await get(url);
    
    return result;
}