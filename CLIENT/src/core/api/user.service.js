import {get, post} from '../utils/httpClient'
import {baseUrl} from '../../environment/environment'

const api = 'user'
export const getUsers = async  () => {
    const url = `${baseUrl}/${api}`;
    const result = await get(url);
    
    return result;
}

export const createUser = async  (user) => {
    const url = `${baseUrl}/${api}`;
    const result = await post(url,user);
    
    return result;
}