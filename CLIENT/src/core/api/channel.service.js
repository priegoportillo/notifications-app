import {post,deletePost} from '../utils/httpClient'
import {baseUrl} from '../../environment/environment'

const api = 'channel'
export const subscribeChannel = async  (userId, channelsIds) => {
    const url = `${baseUrl}/${api}/subscribe`;
    const result = await post(url,{userId, channelsIds});
    return result;
}

export const unSubscribeChannel = async  (userId, channelsIds) => {
    const url = `${baseUrl}/${api}/unsubscribe`;
    const result = await post(url,{userId, channelsIds});
    return result;
}