import {get, post} from '../utils/httpClient'
import {baseUrl} from '../../environment/environment'

const api = 'message'
export const getMessages = async  () => {
    const url = `${baseUrl}/${api}`;
    const result = await get(url);
    
    return result;
}

export const createMessage = async  (message) => {
    const url = `${baseUrl}/${api}`;
    const result = await post(url,message);
    
    return result;
}