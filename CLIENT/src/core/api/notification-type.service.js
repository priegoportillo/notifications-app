import {get} from '../utils/httpClient'
import {baseUrl} from '../../environment/environment'

const api = 'notification-type'

export const getAllNotificationTypes = async  () => {
    const url = `${baseUrl}/${api}`;
    const result = await get(url);
    
    return result;
}