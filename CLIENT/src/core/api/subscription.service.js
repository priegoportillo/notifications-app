import {post,deletePost} from '../utils/httpClient'
import {baseUrl} from '../../environment/environment'

const api = 'subscription'
export const subscribeCategory = async  (userId, categoriesIds) => {
    const url = `${baseUrl}/${api}/subscribe`;
    const result = await post(url,{userId, categoriesIds});
    return result;
}

export const unSubscribeCategory = async  (userId, categoriesIds) => {
    const url = `${baseUrl}/${api}/unsubscribe`;
    const result = await post(url,{userId, categoriesIds});
    return result;
}