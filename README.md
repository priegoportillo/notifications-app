## Software required
```bash
$ Node v18.16.0
$ Docker latest
```

## Docker's installation and execution
```bash
$ cd SERVER
$ docker-compose up -d
```

## Client's installation and execution

```bash
$ cd CLIENT
$ npm install
$ npm run start
```

## Server's nstallation and execution

```bash
$ cd SERVER
$ npm install
$ npm run start:dev
```

