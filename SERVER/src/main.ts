import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { configService } from './config/config.service';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { Transport } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors({
    origin: '*',
    optionsSuccessStatus: 200,
  });

  app.connectMicroservice({
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: 'messages',
        brokers: ['localhost:9092'],
      },
      consumer: {
        groupId: 'messages-consumer',
      },
    },
  });
  const document = SwaggerModule.createDocument(
    app,
    new DocumentBuilder()
      .setTitle('notifications system documentation')
      .setDescription('Documentacion api')
      .setVersion('1.0.0')
      .addTag('Nestjs Swagger UI')
      .build(),
  );
  SwaggerModule.setup('docs', app, document);
  await app.startAllMicroservices();
  await app.listen(configService.getPort());
}
bootstrap();
