import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { UserService } from './user.service';
import { ApiTags } from '@nestjs/swagger';
import { UserCreateDTO } from './models/user.dto';

@ApiTags('user')
@Controller('user')
export class UserController {
  constructor(private readonly service: UserService) {}

  @Get()
  async getAllUsers() {
    try {
      return await this.service.getAll();
    } catch (e) {
      const error = e instanceof Error ? e.message : 'something went wrong!';
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
  @Post()
  async createUser(@Body() dto: UserCreateDTO) {
    try {
      return await this.service.createUser(dto);
    } catch (e) {
      const error = e instanceof Error ? e.message : 'something went wrong!';
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
}
