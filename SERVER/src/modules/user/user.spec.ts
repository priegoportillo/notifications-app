/* eslint-disable @typescript-eslint/camelcase */
import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { INestApplication } from '@nestjs/common';
import { UserModule } from './user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configService } from '../../config/config.service';
import {
    UserCreateDTO,
    UserDTO
} from './models/user.dto';
describe('UserService', () => {
    let service: UserService;
    let app: INestApplication;
    let model: UserDTO;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                UserModule,
                TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
            ],
        }).compile();
        app = module.createNestApplication();
        await app.init();
        service = module.get<UserService>(UserService);
    });

    afterAll(async () => {
        await app.close();
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('shoud be defined model base', () => {
        model = new UserDTO();
        expect(model).toBeDefined();
    });

    it('shoud get valid records', async () => {
        let expectation = 0;
        const users = await service.getAll();
        expect(users.length).toBeGreaterThan(expectation);
    });

    it('shoud be register user', async () => {
        let expectation = 0;
        let newUser = new UserCreateDTO();
        newUser.email = 'user.test@gmail.com';
        newUser.firstName = 'user';
        newUser.lastName = 'test';
        newUser.telephone = '00000000'
        const user = await service.createUser(newUser);
        expect(user.id).toBeDefined();

    });
});
