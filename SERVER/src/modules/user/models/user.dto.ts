import { ApiProperty } from '@nestjs/swagger';
import { Matches, IsString, MaxLength } from 'class-validator';
import { UniqueEntityProp } from '../../../common/validators/unique.validator';
import { UserEntity } from '../../../database/models/user.entity';
import { SubscriptionEntity } from '../../../database/models/subscription.entity';
import { ChannelEntity } from 'src/database/database';

export class UserDTO implements Readonly<UserDTO> {
  id: number;
  fullName: string;
  email: string;
  telephone: string;
  subscriptions: Array<string>;
  channels: Array<string>;
  public static fromEntity(entity: UserEntity) {
    const user = new UserDTO();
    user.id = entity.id;
    user.fullName = `${entity.firstName} ${entity.lastName}`;
    user.email = entity.email;
    user.telephone = entity.telephone;
    user.subscriptions = entity.subscriptions.map(
      (subscription: SubscriptionEntity) => {
        return subscription.category.name;
      },
    );
    user.channels = entity.channels.map((channel: ChannelEntity) => {
      return channel.notificationType.name;
    });
    return user;
  }
}

export class UserCreateDTO {
  @ApiProperty({ description: 'first name' })
  @IsString({ message: 'this first name is not a valid text!' })
  @MaxLength(255, { message: 'max characters 255!' })
  firstName: string;

  @ApiProperty({ description: 'last name' })
  @IsString({ message: 'this last name is not a valid text!' })
  @MaxLength(255, { message: 'max characters 255!' })
  lastName: string;

  @ApiProperty({ description: 'email' })
  @IsString({ message: 'this email is not a valid text!' })
  @MaxLength(255, { message: 'max characters 255!' })
  @Matches(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/, {
    message: 'not a valid email!',
  })
  @UniqueEntityProp(UserEntity, {
    message: 'El campo de email ya ha sido tomado, intente con otro.',
  })
  email: string;

  @ApiProperty({ description: 'telephone' })
  @IsString({ message: 'this telephone is not a valid text!' })
  @MaxLength(255, { message: 'max characters 255!' })
  @Matches(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/, {
    message: 'not a valid telephone!',
  })
  telephone: string;
}

export class UserUpdateDTO {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  telephone: string;
}
