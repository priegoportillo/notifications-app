import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../../database/database';
import { Repository } from 'typeorm';
import { UserDTO, UserCreateDTO } from './models/user.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly repoUser: Repository<UserEntity>,
  ) {}

  async getAll() {
    const users = await this.repoUser
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.subscriptions', 'subscriptions')
      .leftJoinAndSelect('subscriptions.category', 'category')
      .leftJoinAndSelect('user.channels', 'channels')
      .leftJoinAndSelect('channels.notificationType', 'notificationType')
      .where('user.is_delete = :isDelete', { isDelete: false })
      .orderBy('user.id','ASC')
      .getMany();
    return users.map(user => {
      return UserDTO.fromEntity(user);
    });
  }

  async createUser(user: UserCreateDTO) {
    const newUser = new UserEntity();
    newUser.email = user.email;
    newUser.firstName = user.firstName;
    newUser.lastName = user.lastName;
    newUser.telephone = user.telephone;
    const saveStatus = await this.repoUser.save(newUser);
    return saveStatus;
  }
}
