import {
  Controller,
  Post,
  Body,
  HttpException,
  HttpStatus,
  Inject,
  Get,
} from '@nestjs/common';
import { MessageService } from './message.service';
import { ApiTags } from '@nestjs/swagger';
import { MessageCreateDTO } from './models/message.dto';
import { ClientProxy, MessagePattern, Payload } from '@nestjs/microservices';

@ApiTags('message')
@Controller('message')
export class MessageController {
  constructor(
    private readonly service: MessageService,
    @Inject('MESSAGES_SERVICE')
    private readonly messagesClient: ClientProxy,
  ) {}

  @Post()
  async createMessage(@Body() dto: MessageCreateDTO) {
    try {
      const messageStatus = await this.service.createMessage(dto);
      return this.messagesClient.emit('message.created', {
        messageStatus,
      });
    } catch (e) {
      const error = e instanceof Error ? e.message : 'something went wrong!';
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
  
  @Get()
  async getAllMessages() {
    try {
      return await this.service.getAll();
    } catch (e) {
      const error = e instanceof Error ? e.message : 'something went wrong!';
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
}
