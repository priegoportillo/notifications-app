import { Module } from '@nestjs/common';
import { MessageController } from './message.controller';
import { MessageService } from './message.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { entities } from '../../database/database';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [
    TypeOrmModule.forFeature([...entities]),
    ClientsModule.register([
      {
        name: 'MESSAGES_SERVICE',
        transport: Transport.KAFKA,
        options: {
          client: {
            clientId: 'messages',
            brokers: ['localhost:9092'],
          },
          consumer: {
            groupId: 'messages-consumer',
          },
        },
      },
    ]),
  ],
  controllers: [MessageController],
  providers: [MessageService],
  exports: [MessageService],
})
export class MessageModule {}
