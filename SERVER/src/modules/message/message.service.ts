import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoryEntity, MessageEntity } from '../../database/database';
import { Repository } from 'typeorm';
import { MessageCreateDTO, MessageDTO } from './models/message.dto';

@Injectable()
export class MessageService {
  constructor(
    @InjectRepository(MessageEntity)
    private readonly repoMessage: Repository<MessageEntity>,
    @InjectRepository(CategoryEntity)
    private readonly repoCategory: Repository<CategoryEntity>,
  ) {}

  async createMessage(dto: MessageCreateDTO) {
    const newMessage = new MessageEntity();
    newMessage.content = dto.content;
    newMessage.category = await this.repoCategory.findOne({
      where: {
        id: dto.categoryId,
      },
    });

    const saveStatus = await this.repoMessage.save(newMessage);
    return saveStatus;
  }

  async getAll() {
    const messages = await this.repoMessage
      .createQueryBuilder('message')
      .innerJoinAndSelect('message.category','category')
      .where('category.is_delete = :isDelete', { isDelete: false })
      .getMany();
    return messages.map(message => {
      return MessageDTO.fromEntity(message);
    });
  }
}
