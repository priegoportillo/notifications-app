/* eslint-disable @typescript-eslint/camelcase */
import { Test, TestingModule } from '@nestjs/testing';
import { MessageService } from './message.service';
import { INestApplication } from '@nestjs/common';
import { MessageModule } from './message.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configService } from '../../config/config.service';
import {
    MessageCreateDTO,
    MessageDTO
} from './models/message.dto';
describe('MessageService', () => {
    let service: MessageService;
    let app: INestApplication;
    let model: MessageDTO;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                MessageModule,
                TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
            ],
        }).compile();
        app = module.createNestApplication();
        await app.init();
        service = module.get<MessageService>(MessageService);
    });

    afterAll(async () => {
        await app.close();
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('shoud be defined model base', () => {
        model = new MessageDTO();
        expect(model).toBeDefined();
    });

    it('shoud get valid records', async () => {
        let expectation = 0;
        const Messages = await service.getAll();
        expect(Messages.length).toBeGreaterThan(expectation);
    });

    it('shoud be register Message', async () => {
        let newMessage = new MessageCreateDTO();
        newMessage.content = 'message test';
        newMessage.categoryId = 1;
        const Message = await service.createMessage(newMessage);
        expect(Message.id).toBeDefined();

    });
});
