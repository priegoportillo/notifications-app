import { ApiProperty } from '@nestjs/swagger';
import { IsString, MaxLength } from 'class-validator';
import { MessageEntity } from 'src/database/database';

export class MessageCreateDTO implements Readonly<MessageCreateDTO> {
  @ApiProperty({ description: 'category' })
  categoryId: number;

  @ApiProperty({ description: 'content' })
  @IsString()
  @MaxLength(255, { message: 'max characters 255!' })
  content: string;
}

export class MessageDTO implements Readonly<MessageDTO> {
  id: number;
  content: string;
  category: string;

  public static fromEntity(entity: MessageEntity) {
    const message = new MessageDTO();
    message.id = entity.id;
    message.content = entity.content;
    message.category = entity.category.name;
    return message;
  }
}
