import { ApiProperty } from '@nestjs/swagger';
import { IsArray, ArrayMaxSize } from 'class-validator';

export class SubscribeDTO implements Readonly<SubscribeDTO> {
  @ApiProperty({ description: 'user' })
  userId: number;

  @ApiProperty({ description: 'subscriptions' })
  @IsArray()
  @ArrayMaxSize(3)
  channelsIds: Array<number>;
}

export class UnSubscribeDTO implements Readonly<UnSubscribeDTO> {
  @ApiProperty({ description: 'user' })
  userId: number;

  @ApiProperty({ description: 'subscriptions' })
  @IsArray()
  @ArrayMaxSize(3)
  channelsIds: Array<number>;
}
