import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  ChannelEntity,
  NotificationTypeEntity,
  UserEntity,
} from '../../database/database';
import { Repository } from 'typeorm';
import { SubscribeDTO, UnSubscribeDTO } from './models/channel.dto';

@Injectable()
export class ChannelService {
  constructor(
    @InjectRepository(ChannelEntity)
    private readonly repoChannel: Repository<ChannelEntity>,
    @InjectRepository(UserEntity)
    private readonly repoUser: Repository<UserEntity>,
    @InjectRepository(NotificationTypeEntity)
    private readonly repoNotificatinType: Repository<NotificationTypeEntity>,
  ) {}

  async subscribe(dto: SubscribeDTO) {
    const notificationsTypes = await this.repoNotificatinType
      .createQueryBuilder('notificationTypes')
      .where('id IN(:...ids)', {
        ids: dto.channelsIds,
      })
      .getMany();
    const user = await this.repoUser
      .createQueryBuilder('user')
      .where('id = :id', {
        id: dto.userId,
      })
      .getOne();
    const Channels = notificationsTypes.map(notificationType => {
      return {
        userId: user.id,
        notificationTypeId: notificationType.id,
      };
    });
    return await this.repoChannel.save(Channels);
  }

  async unSubscribe(dto: UnSubscribeDTO) {
    const unSubscribes = await this.repoChannel
      .createQueryBuilder('Channel')
      .delete()
      .where('user_id = :userId AND notificationTypeId IN(:...channelsIds)', {
        userId: dto.userId,
        channelsIds: dto.channelsIds,
      })
      .execute();
    return unSubscribes;
  }
}
