/* eslint-disable @typescript-eslint/camelcase */
import { Test, TestingModule } from '@nestjs/testing';
import { ChannelService } from './channel.service';
import { INestApplication } from '@nestjs/common';
import { ChannelModule } from './channel.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configService } from '../../config/config.service';
import {
    SubscribeDTO,
    UnSubscribeDTO
} from './models/channel.dto';

describe('ChannelService', () => {
    let service: ChannelService;
    let app: INestApplication;
    let model: SubscribeDTO;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ChannelModule,
                TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
            ],
        }).compile();
        app = module.createNestApplication();
        await app.init();
        service = module.get<ChannelService>(ChannelService);
    });

    afterAll(async () => {
        await app.close();
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('shoud be defined model base', () => {
        model = new SubscribeDTO();
        expect(model).toBeDefined();
    });

    it('should be a new channels subscription', async () => {
        const expectation = 1;
        const DTO = new SubscribeDTO();
        DTO.channelsIds = [1]
        DTO.userId = 1;
        const subscribed = await service.subscribe(DTO);
        expect(subscribed.length).toBe(expectation);
    });

    it('should be a new channels unSubscription', async () => {
        const expectation = 1;
        const DTO = new UnSubscribeDTO();
        DTO.channelsIds = [1]
        DTO.userId = 1;
        const unSubscribed = await service.unSubscribe(DTO);
        expect(unSubscribed.affected).toBe(expectation);
    });
});
