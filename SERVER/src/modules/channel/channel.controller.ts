import {
  Controller,
  Post,
  Delete,
  Body,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { ChannelService } from './channel.service';
import { ApiTags } from '@nestjs/swagger';
import { SubscribeDTO, UnSubscribeDTO } from './models/channel.dto';

@ApiTags('channel')
@Controller('channel')
export class ChannelController {
  constructor(private readonly service: ChannelService) {}

  @Post('subscribe')
  async subscribe(@Body() dto: SubscribeDTO) {
    try {
      return await this.service.subscribe(dto);
    } catch (e) {
      const error = e instanceof Error ? e.message : 'something went wrong!';
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
  @Post('unsubscribe')
  async unSubscribe(@Body() dto: UnSubscribeDTO) {
    try {
      return await this.service.unSubscribe(dto);
    } catch (e) {
      const error = e instanceof Error ? e.message : 'something went wrong!';
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
}
