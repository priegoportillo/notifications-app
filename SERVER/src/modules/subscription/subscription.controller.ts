import {
  Controller,
  Post,
  Delete,
  Body,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { SubscriptionService } from './subscription.service';
import { ApiTags } from '@nestjs/swagger';
import { SubscribeDTO, UnSubscribeDTO } from './models/subscription.dto';

@ApiTags('subscription')
@Controller('subscription')
export class SubscriptionController {
  constructor(private readonly service: SubscriptionService) {}

  @Post('subscribe')
  async subscribe(@Body() dto: SubscribeDTO) {
    try {
      return await this.service.subscribe(dto);
    } catch (e) {
      const error = e instanceof Error ? e.message : 'something went wrong!';
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
  @Post('unsubscribe')
  async unSubscribe(@Body() dto: UnSubscribeDTO) {
    try {
      return await this.service.unSubscribe(dto);
    } catch (e) {
      const error = e instanceof Error ? e.message : 'something went wrong!';
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
}
