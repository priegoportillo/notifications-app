import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  CategoryEntity,
  SubscriptionEntity,
  UserEntity,
} from '../../database/database';
import { Repository } from 'typeorm';
import { SubscribeDTO, UnSubscribeDTO } from './models/subscription.dto';

@Injectable()
export class SubscriptionService {
  constructor(
    @InjectRepository(SubscriptionEntity)
    private readonly repoSubscription: Repository<SubscriptionEntity>,
    @InjectRepository(UserEntity)
    private readonly repoUser: Repository<UserEntity>,
    @InjectRepository(CategoryEntity)
    private readonly repoCategory: Repository<CategoryEntity>,
  ) {}

  async subscribe(dto: SubscribeDTO) {
    const categories = await this.repoCategory
      .createQueryBuilder('category')
      .where('id IN(:...ids)', {
        ids: dto.categoriesIds,
      })
      .getMany();
    const user = await this.repoUser
      .createQueryBuilder('user')
      .where('id = :id', {
        id: dto.userId,
      })
      .getOne();
    const subscriptions = categories.map(category => {
      return {
        userId: user.id,
        categoryId: category.id,
      };
    });
    return await this.repoSubscription.save(subscriptions);
  }

  async unSubscribe(dto: UnSubscribeDTO) {
    const unSubscribes = this.repoSubscription
      .createQueryBuilder('subscription')
      .delete()
      .where('user_id = :userId AND category_id IN(:...categoriesIds)', {
        userId: dto.userId,
        categoriesIds: dto.categoriesIds,
      })
      .execute();
    return unSubscribes;
  }
}
