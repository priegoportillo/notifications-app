/* eslint-disable @typescript-eslint/camelcase */
import { Test, TestingModule } from '@nestjs/testing';
import { SubscriptionService } from './subscription.service';
import { INestApplication } from '@nestjs/common';
import { SubscriptionModule } from './subscription.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configService } from '../../config/config.service';
import {
    SubscribeDTO,
    UnSubscribeDTO
} from './models/subscription.dto';

describe('SubscriptionService', () => {
    let service: SubscriptionService;
    let app: INestApplication;
    let model: SubscribeDTO;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                SubscriptionModule,
                TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
            ],
        }).compile();
        app = module.createNestApplication();
        await app.init();
        service = module.get<SubscriptionService>(SubscriptionService);
    });

    afterAll(async () => {
        await app.close();
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('shoud be defined model base', () => {
        model = new SubscribeDTO();
        expect(model).toBeDefined();
    });

    it('should be a new categories subscription', async () => {
        const expectation = 1;
        const DTO = new SubscribeDTO();
        DTO.categoriesIds = [1]
        DTO.userId = 1;
        const subscribed = await service.subscribe(DTO);
        expect(subscribed.length).toBe(expectation);
    });

    it('should be a new categories unSubscription', async () => {
        const expectation = 1;
        const DTO = new UnSubscribeDTO();
        DTO.categoriesIds = [1]
        DTO.userId = 1;
        const unSubscribed = await service.unSubscribe(DTO);
        expect(unSubscribed.affected).toBe(expectation);
    });
});
