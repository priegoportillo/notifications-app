import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { NotificationEntity, UserEntity } from '../../database/database';
import { Repository } from 'typeorm';
import {
  NotifyWithEmailStrategy,
  NotifyWithSMSStrategy,
  NotifyWithOnPushStrategy,
  NotificationContext,
} from './notification.strategy';
import { NotificationDTO } from './models/notification.dto';

@Injectable()
export class NotificationService {
  constructor(
    @InjectRepository(NotificationEntity)
    private readonly repoNotifications: Repository<NotificationEntity>,
    @InjectRepository(UserEntity)
    private readonly repoUser: Repository<UserEntity>,
  ) {}

  async createNotifications(dto: any) {
    const strategies = [
      new NotifyWithEmailStrategy(),
      new NotifyWithSMSStrategy(),
      new NotifyWithOnPushStrategy(),
    ];

    const strategy = new NotificationContext(strategies[0]);

    const subscriptors = await this.repoUser
      .createQueryBuilder('user')
      .leftJoin('user.subscriptions', 'subscriptions')
      .where('subscriptions.category_id = :categoryId', {
        categoryId: dto.messageStatus.category.id,
      })
      .leftJoinAndSelect('user.channels', 'channels')
      .getMany();
    const notifications = subscriptors.map(subscriptor => {
      return subscriptor.channels.map(channel => {
        strategy.setStrategy(strategies[channel.notificationTypeId - 1]);
        strategy.send(dto.messageStatus.content,subscriptor.email);
        return {
          userId: subscriptor.id,
          categoryId: dto.messageStatus.category.id,
          notificationTypeId: channel.notificationTypeId,
          messageId: dto.messageStatus.id,
        };
      });
    });
    await this.repoNotifications.save([].concat(...notifications));
  }

  async getAllNotificationsByUser(userId:number){
    const notifications = await this.repoNotifications
    .createQueryBuilder('notification')
    .innerJoin('notification.user','user')
    .innerJoin('notification.category','category')
    .innerJoin('notification.channel','channel')
    .innerJoin('notification.message','message')
    .addSelect(['user.firstName'
    ,'user.lastName'
    ,'channel.name'
    ,'message.content'
    ,'category.name'])
    .where('user_id = :userId',{userId:userId})
    .orderBy('log_date','DESC')
    .getMany();

    return notifications.map((notification)=>{
      return NotificationDTO.fromEntity(notification);
    });
  }
}
