import { Injectable } from '@nestjs/common';

interface NotificationStrategy {
  send(content: string,user:string): boolean;
}
@Injectable()
export class NotificationContext {
  private strategy: NotificationStrategy;

  constructor(strategy: NotificationStrategy) {
    this.setStrategy(strategy);
  }
  setStrategy(strategy: NotificationStrategy) {
    this.strategy = strategy;
  }
  send(content: string,user:string) {
    const status = this.strategy.send(content,user);
    return status;
  }
}

@Injectable()
export class NotifyWithEmailStrategy implements NotificationStrategy {
  send(content: string,user:string) {
    console.log(`${user} sending by email`);
    return true;
  }
}
@Injectable()
export class NotifyWithSMSStrategy implements NotificationStrategy {
  send(content: string,user:string) {
    console.log(`${user} sending by SMS`);
    return true;
  }
}
@Injectable()
export class NotifyWithOnPushStrategy implements NotificationStrategy {
  send(content: string,user:string) {
    console.log(`${user} sending by on push`);
    return true;
  }
}
