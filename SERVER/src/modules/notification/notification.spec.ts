/* eslint-disable @typescript-eslint/camelcase */
import { Test, TestingModule } from '@nestjs/testing';
import { NotificationService } from './notification.service';
import { INestApplication } from '@nestjs/common';
import { NotificationModule } from './notification.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configService } from '../../config/config.service';
import {
    NotificationDTO
} from './models/notification.dto';
import {
    NotifyWithEmailStrategy,
    NotifyWithSMSStrategy,
    NotifyWithOnPushStrategy,
    NotificationContext,
} from './notification.strategy';

describe('NotificationService', () => {
    let service: NotificationService;
    let app: INestApplication;
    let model: NotificationDTO;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                NotificationModule,
                TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
            ],
        }).compile();
        app = module.createNestApplication();
        await app.init();
        service = module.get<NotificationService>(NotificationService);
    });

    afterAll(async () => {
        await app.close();
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('shoud be defined model base', () => {
        model = new NotificationDTO();
        expect(model).toBeDefined();
    });

    it('shoud get valid records', async () => {
        let expectation = 0;
        let userID = 1;
        const Notifications = await service.getAllNotificationsByUser(userID);
        expect(Notifications.length).toBeGreaterThan(expectation);
    });

    // it('shoud be register Notification', async () => {
    //     let newNotification = new NotificationCreateDTO();
    //     newNotification.content = 'Notification test';
    //     newNotification.categoryId = 1;
    //     const Notification = await service.createNotification(newNotification);
    //     expect(Notification.id).toBeDefined();

    // });

    it('shoud be a valid notification by SMS', async () => {
        const expectation = true;
        const strategy =new NotificationContext(new NotifyWithSMSStrategy());
        let status = strategy.send('content for testing','user.test@gmail.com');
        expect(status).toBe(expectation);
    });

    it('shoud be a valid notification by E-Mail', async () => {
        const expectation = true;
        const strategy =new NotificationContext(new NotifyWithEmailStrategy());
        let status = strategy.send('content for testing','user.test@gmail.com');
        expect(status).toBe(expectation);
    });

    it('shoud be a valid notification by Push Notification', async () => {
        const expectation = true;
        const strategy =new NotificationContext(new NotifyWithOnPushStrategy());
        let status = strategy.send('content for testing','user.test@gmail.com');
        expect(status).toBe(expectation);
    });
});
