import { NotificationEntity } from "src/database/database";

export class NotificationDTO
  implements Readonly<NotificationDTO> {
  logDate: Date;
  fullName: string;
  content: string;
  subscription: string;
  channel: string;

  public static fromEntity(entity: NotificationEntity) {
    const notification = new NotificationDTO();
    notification.content = entity.message.content;
    notification.subscription = entity.category.name;
    notification.channel = entity.channel.name;
    notification.logDate = entity.logDate;
    return notification;
  }
}
