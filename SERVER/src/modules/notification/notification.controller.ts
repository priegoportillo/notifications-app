import { Controller, Get, HttpException, HttpStatus, Param } from '@nestjs/common';
import { NotificationService } from './notification.service';
import { ApiTags } from '@nestjs/swagger';
import { MessagePattern, Payload } from '@nestjs/microservices';

@ApiTags('notification')
@Controller('notification')
export class NotificationController {
  constructor(private readonly service: NotificationService) {}

  @MessagePattern('message.created')
  async notify(@Payload() message: any) {
    try {
      await this.service.createNotifications(message.value);
    } catch (e) {
      const error = e instanceof Error ? e.message : 'something went wrong!';
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  @Get('user/:userId')
  async getNotificationsByUserId(@Param('userId')userId:number){
    try {
      return await this.service.getAllNotificationsByUser(userId);
    } catch (e) {
      const error = e instanceof Error ? e.message : 'something went wrong!';
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
}
