import { Module } from '@nestjs/common';
import { NotificationTypeController } from './notification-type.controller';
import { NotificationTypeService } from './notification-type.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { entities } from '../../database/database';

@Module({
  imports: [TypeOrmModule.forFeature([...entities])],
  controllers: [NotificationTypeController],
  providers: [NotificationTypeService],
  exports: [NotificationTypeService],
})
export class NotificationTypeModule {}
