import { Controller, Get, HttpException, HttpStatus } from '@nestjs/common';
import { NotificationTypeService } from './notification-type.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('notification-type')
@Controller('notification-type')
export class NotificationTypeController {
  constructor(private readonly service: NotificationTypeService) {}

  @Get()
  async getAllCategories() {
    try {
      return await this.service.getAll();
    } catch (e) {
      const error = e instanceof Error ? e.message : 'something went wrong!';
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
}
