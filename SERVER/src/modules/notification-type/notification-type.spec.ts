/* eslint-disable @typescript-eslint/camelcase */
import { Test, TestingModule } from '@nestjs/testing';
import { NotificationTypeService } from './notification-type.service';
import { INestApplication } from '@nestjs/common';
import { NotificationTypeModule } from './notification-type.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configService } from '../../config/config.service';
import {
    NotificationTypeDTO
} from './models/notification-type.dto';
describe('NotificationTypeService', () => {
    let service: NotificationTypeService;
    let app: INestApplication;
    let model: NotificationTypeDTO;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                NotificationTypeModule,
                TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
            ],
        }).compile();
        app = module.createNestApplication();
        await app.init();
        service = module.get<NotificationTypeService>(NotificationTypeService);
    });

    afterAll(async () => {
        await app.close();
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('shoud be defined model base', () => {
        model = new NotificationTypeDTO();
        expect(model).toBeDefined();
    });

    it('shoud get valid records', async () => {
        const notificationsTypes = await service.getAll();
        expect(notificationsTypes.length).toBe(3);
    });
});
