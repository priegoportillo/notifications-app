import { NotificationTypeEntity } from '../../../database/models/notification-type.entity';

export class NotificationTypeDTO implements Readonly<NotificationTypeDTO> {
  id: number;
  name: string;

  public static fromEntity(entity: NotificationTypeEntity) {
    const channel = new NotificationTypeDTO();
    channel.id = entity.id;
    channel.name = entity.name;
    return channel;
  }
}
