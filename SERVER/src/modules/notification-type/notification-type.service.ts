import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { NotificationTypeEntity } from '../../database/database';
import { Repository } from 'typeorm';
import { NotificationTypeDTO } from './models/notification-type.dto';

@Injectable()
export class NotificationTypeService {
  constructor(
    @InjectRepository(NotificationTypeEntity)
    private readonly repoNotificationType: Repository<NotificationTypeEntity>,
  ) {}

  async getAll() {
    const notificationsTypes = await this.repoNotificationType
      .createQueryBuilder('notificationType')
      .where('notificationType.is_delete = :isDelete', { isDelete: false })
      .getMany();
    return notificationsTypes.map(notificationType => {
      return NotificationTypeDTO.fromEntity(notificationType);
    });
  }
}
