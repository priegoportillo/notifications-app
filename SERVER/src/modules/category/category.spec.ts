/* eslint-disable @typescript-eslint/camelcase */
import { Test, TestingModule } from '@nestjs/testing';
import { CategoryService } from './category.service';
import { INestApplication } from '@nestjs/common';
import { CategoryModule } from './category.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configService } from '../../config/config.service';
import {
    CategoryDTO
} from './models/category.dto';
describe('CategoryService', () => {
    let service: CategoryService;
    let app: INestApplication;
    let model: CategoryDTO;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                CategoryModule,
                TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
            ],
        }).compile();
        app = module.createNestApplication();
        await app.init();
        service = module.get<CategoryService>(CategoryService);
    });

    afterAll(async () => {
        await app.close();
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('shoud be defined model base', () => {
        model = new CategoryDTO();
        expect(model).toBeDefined();
    });

    it('shoud get valid records', async () => {
        const categories = await service.getAll();
        expect(categories.length).toBe(3);
    });
});
