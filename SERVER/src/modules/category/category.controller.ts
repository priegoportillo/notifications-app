import { Controller, Get, HttpException, HttpStatus } from '@nestjs/common';
import { CategoryService } from './category.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('category')
@Controller('category')
export class CategoryController {
  constructor(private readonly service: CategoryService) {}

  @Get()
  async getAllCategories() {
    try {
      return await this.service.getAll();
    } catch (e) {
      const error = e instanceof Error ? e.message : 'something went wrong!';
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
}
