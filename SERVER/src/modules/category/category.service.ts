import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoryEntity } from '../../database/database';
import { Repository } from 'typeorm';
import { CategoryDTO } from './models/category.dto';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(CategoryEntity)
    private readonly repoCategory: Repository<CategoryEntity>,
  ) {}

  async getAll() {
    const categories = await this.repoCategory
      .createQueryBuilder('category')
      .where('category.is_delete = :isDelete', { isDelete: false })
      .getMany();
    return categories.map(category => {
      return CategoryDTO.fromEntity(category);
    });
  }
}
