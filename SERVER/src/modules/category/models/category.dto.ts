import { CategoryEntity } from '../../../database/models/category.entity';

export class CategoryDTO implements Readonly<CategoryDTO> {
  id: number;
  name: string;

  public static fromEntity(entity: CategoryEntity) {
    const channel = new CategoryDTO();
    channel.id = entity.id;
    channel.name = entity.name;
    return channel;
  }
}
