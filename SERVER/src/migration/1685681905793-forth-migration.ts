import {MigrationInterface, QueryRunner} from "typeorm";

export class forthMigration1685681905793 implements MigrationInterface {
    name = 'forthMigration1685681905793'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "subscription" DROP CONSTRAINT "FK_aa529383c3279195026c21420ed"`);
        await queryRunner.query(`ALTER TABLE "subscription" DROP COLUMN "channel_id"`);
        await queryRunner.query(`ALTER TABLE "subscription" ADD CONSTRAINT "FK_e7ddc1213ce93c60338e6da254d" FOREIGN KEY ("category_id") REFERENCES "category"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "subscription" DROP CONSTRAINT "FK_e7ddc1213ce93c60338e6da254d"`);
        await queryRunner.query(`ALTER TABLE "subscription" ADD "channel_id" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "subscription" ADD CONSTRAINT "FK_aa529383c3279195026c21420ed" FOREIGN KEY ("channel_id") REFERENCES "category"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
