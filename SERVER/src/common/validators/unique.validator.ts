import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { getManager } from 'typeorm';

/**
 * unique entity property constraint
 */
@ValidatorConstraint({ async: true })
export class UniqueEntityPropConstraint
  implements ValidatorConstraintInterface {
  /**
   * validate method
   * @param value
   * @param args
   */
  async validate(value: any, args: ValidationArguments) {
    const entity = args.object[`class_entity_${args.property}`];
    return getManager()
      .count(entity, { [args.property]: value, isDelete: false })
      .then(count => count < 1);
  }
}

/**
 * verify element if the property into entity exists
 * @param entity
 * @param validationOptions
 */
export function UniqueEntityProp(
  entity: Function,
  validationOptions?: ValidationOptions,
) {
  validationOptions = {
    ...{ message: '$value already exists. Choose another.' },
    ...validationOptions,
  };
  return function(object: Record<string, any>, propertyName: string) {
    object[`class_entity_${propertyName}`] = entity;
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: UniqueEntityPropConstraint,
    });
  };
}
