import { TypeOrmModuleOptions } from '@nestjs/typeorm';
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

class ConfigService {
  constructor(private env: { [k: string]: string | undefined }) {}

  private getValue(key: string, throwOnMissing = true): string {
    const value = this.env[key];
    if (!value && throwOnMissing) {
      throw new Error(`config error - missing env.${key}`);
    }
    return value;
  }

  public ensureValues(keys: string[]) {
    keys.forEach((k) => this.getValue(k, true));
    return this;
  }

  public getPort() {
    return this.getValue('PORT', true);
  }

  public isProduction() {
    const mode = this.getValue('MODE', false);
    return mode != 'DEV';
  }

  public isDbSSL() {
    const mode = this.getValue('POSTGRES_SSL', false);
    return mode == 'true';
  }

  public getMailUser() {
    return this.getValue('MAIL_USER', true);
  }

  public getMailPassword() {
    return this.getValue('MAIL_PASS', true);
  }

  public getWebSocketPort(): number {
    const value = this.getValue('WEB_SOCKET', true);
    if (isNaN(Number(value))) {
      throw new Error('invalida websocket port');
    }
    return Number(value);
  }

  public getTypeOrmConfig(): TypeOrmModuleOptions {
    const entitiesPath = this.isProduction() ? 'dist' : 'src';
    const entitiesExtension = this.isProduction() ? '.js' : '{.ts,.js}';
    const migrationPath = this.isProduction() ? 'dist' : 'src';
    const migrationExtension = this.isProduction() ? '.js' : '.ts';

    return {
      type: 'postgres',
      host: this.getValue('POSTGRES_HOST'),
      port: parseInt(this.getValue('POSTGRES_PORT')),
      username: this.getValue('POSTGRES_USER'),
      password: this.getValue('POSTGRES_PASSWORD'),
      database: this.getValue('POSTGRES_DATABASE'),
      entities: [`${entitiesPath}/**/*.entity${entitiesExtension}`],
      migrationsTableName: 'migration',
      migrations: [`${migrationPath}/migration/*${migrationExtension}`],
      ssl: this.isDbSSL(),
      synchronize: this.isProduction() ? false : true,
    };
  }
}

const configService = new ConfigService(process.env).ensureValues([
  'POSTGRES_HOST',
  'POSTGRES_PORT',
  'POSTGRES_USER',
  'POSTGRES_PASSWORD',
  'POSTGRES_DATABASE',
  'POSTGRES_SSL',
]);

export { configService };
