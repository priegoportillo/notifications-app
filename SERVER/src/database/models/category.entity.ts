import { BaseEntity } from './base.entity';
import { Column, Entity, OneToMany } from 'typeorm';
import { MessageEntity } from './message.entity';

@Entity({
  name: 'category',
  schema: 'public',
})
export class CategoryEntity extends BaseEntity {
  @Column({
    name: 'name',
    type: 'varchar',
    length: 255,
    nullable: false,
  })
  name: string;

  @OneToMany(
    () => MessageEntity,
    e => e.category,
  )
  messages: MessageEntity[];
}
