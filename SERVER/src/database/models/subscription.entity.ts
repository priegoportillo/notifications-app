import { Entity, ManyToOne, JoinColumn, PrimaryColumn } from 'typeorm';
import { UserEntity } from './user.entity';
import { CategoryEntity } from './category.entity';

@Entity({
  name: 'subscription',
  schema: 'public',
})
export class SubscriptionEntity {
  @PrimaryColumn({
    name: 'user_id',
  })
  userId: number;

  /**
   * @field fk file table primary key
   */
  @PrimaryColumn({
    name: 'category_id',
  })
  categoryId: number;

  /**
   * @virtual relation with document table
   */
  @ManyToOne(
    () => UserEntity,
    r => r.id,
    {
      nullable: false,
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION',
    },
  )
  @JoinColumn({ name: 'user_id' })
  user?: UserEntity | null;

  /**
   * @virtual relation with file table
   */
  @ManyToOne(
    () => CategoryEntity,
    a => a.id,
    {
      nullable: false,
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION',
    },
  )
  @JoinColumn({ name: 'category_id' })
  category?: CategoryEntity | null;
}
