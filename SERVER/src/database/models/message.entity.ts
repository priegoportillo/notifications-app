import { BaseEntity } from './base.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { CategoryEntity } from './category.entity';

@Entity({
  name: 'message',
  schema: 'public',
})
export class MessageEntity extends BaseEntity {
  @Column({
    name: 'content',
    type: 'varchar',
    length: 255,
    nullable: false,
  })
  content: string;

  @ManyToOne(
    () => CategoryEntity,
    e => e.messages,
  )
  @JoinColumn({ name: 'Category_id' })
  category: CategoryEntity;
}
