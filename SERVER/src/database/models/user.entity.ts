import { BaseEntity } from './base.entity';
import { Column, Entity, OneToMany } from 'typeorm';
import { SubscriptionEntity } from './subscription.entity';
import { NotificationTypeEntity } from './notification-type.entity';
import { ChannelEntity } from './channel.entity';

@Entity({
  name: 'user',
  schema: 'public',
})
export class UserEntity extends BaseEntity {
  @Column({
    name: 'first_name',
    type: 'varchar',
    length: 255,
    nullable: false,
  })
  firstName: string;

  @Column({
    name: 'last_name',
    type: 'varchar',
    length: 255,
    nullable: false,
  })
  lastName: string;

  @Column({
    name: 'email',
    type: 'varchar',
    length: 255,
    nullable: false,
  })
  email: string;

  @Column({
    name: 'telephone',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  telephone: string;

  @OneToMany(
    () => SubscriptionEntity,
    e => e.user,
  )
  subscriptions: SubscriptionEntity[];

  @OneToMany(
    () => ChannelEntity,
    e => e.user,
  )
  channels: ChannelEntity[];
}
