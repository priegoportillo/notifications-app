import { BaseEntity } from './base.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { NotificationEntity } from './notification.entity';
import { UserEntity } from './user.entity';

@Entity({
  name: 'notification_type',
  schema: 'public',
})
export class NotificationTypeEntity extends BaseEntity {
  @Column({
    name: 'name',
    type: 'varchar',
    length: 255,
    nullable: false,
  })
  name: string;
}
