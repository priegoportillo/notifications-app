import { Entity, ManyToOne, JoinColumn, PrimaryColumn } from 'typeorm';
import { UserEntity } from './user.entity';
import { CategoryEntity } from './category.entity';
import { NotificationTypeEntity } from './notification-type.entity';

@Entity({
  name: 'channel',
  schema: 'public',
})
export class ChannelEntity {
  @PrimaryColumn({
    name: 'user_id',
  })
  userId: number;

  /**
   * @field fk file table primary key
   */
  @PrimaryColumn({
    name: 'notification_type_id',
  })
  notificationTypeId: number;

  /**
   * @virtual relation with document table
   */
  @ManyToOne(
    () => UserEntity,
    r => r.id,
    {
      nullable: false,
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION',
    },
  )
  @JoinColumn({ name: 'user_id' })
  user?: UserEntity | null;

  /**
   * @virtual relation with file table
   */
  @ManyToOne(
    () => NotificationTypeEntity,
    a => a.id,
    {
      nullable: false,
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION',
    },
  )
  @JoinColumn({ name: 'notification_type_id' })
  notificationType?: NotificationTypeEntity | null;
}
