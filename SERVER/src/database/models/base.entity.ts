import { Column, PrimaryGeneratedColumn } from 'typeorm';

export abstract class BaseEntity {
  /**
   * @field identifier row table
   */
  @PrimaryGeneratedColumn({ name: 'id' })
  id: number;

  /**
   * @field is_delete
   * @summary indica si la entidad esta eliminada.
   * */
  @Column({
    name: 'is_delete',
    type: 'boolean',
    default: false,
  })
  isDelete: boolean;
}
