import { BaseEntity } from './base.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
} from 'typeorm';
import { NotificationTypeEntity } from './notification-type.entity';
import { UserEntity } from './user.entity';
import { CategoryEntity } from './category.entity';
import { MessageEntity } from './message.entity';

@Entity({
  name: 'notification',
  schema: 'public',
})
export class NotificationEntity extends BaseEntity {
  @PrimaryColumn({
    name: 'user_id',
  })
  userId: number;

  @PrimaryColumn({
    name: 'category_id',
  })
  categoryId: number;

  @PrimaryColumn({
    name: 'notification_type_id',
  })
  notificationTypeId: number;

  @PrimaryColumn({
    name: 'message_id',
  })
  messageId: number;

  @CreateDateColumn({
    name: 'log_date',
    type: 'timestamptz',
    default: new Date(),
  })
  logDate: Date;

  //Virtual relationships
  @ManyToOne(
    () => UserEntity,
    r => r.id,
    {
      nullable: false,
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION',
    },
  )
  @JoinColumn({ name: 'user_id' })
  user?: UserEntity | null;

  @ManyToOne(
    () => CategoryEntity,
    r => r.id,
    {
      nullable: false,
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION',
    },
  )
  @JoinColumn({ name: 'category_id' })
  category?: CategoryEntity | null;

  @ManyToOne(
    () => NotificationTypeEntity,
    r => r.id,
    {
      nullable: false,
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION',
    },
  )
  @JoinColumn({ name: 'notification_type_id' })
  channel?: NotificationTypeEntity | null;

  @ManyToOne(
    () => MessageEntity,
    r => r.id,
    {
      nullable: false,
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION',
    },
  )
  @JoinColumn({ name: 'message_id' })
  message?: MessageEntity | null;
}
