import { UserEntity } from './models/user.entity';
import { CategoryEntity } from './models/category.entity';
import { MessageEntity } from './models/message.entity';
import { SubscriptionEntity } from './models/subscription.entity';
import { NotificationEntity } from './models/notification.entity';
import { NotificationTypeEntity } from './models/notification-type.entity';
import { ChannelEntity } from './models/channel.entity';

export const entities = [
  UserEntity,
  CategoryEntity,
  MessageEntity,
  SubscriptionEntity,
  NotificationEntity,
  NotificationTypeEntity,
  ChannelEntity,
  MessageEntity,
];
export {
  UserEntity,
  CategoryEntity,
  SubscriptionEntity,
  NotificationEntity,
  NotificationTypeEntity,
  ChannelEntity,
  MessageEntity,
};
