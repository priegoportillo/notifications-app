--
-- PostgreSQL database dump
--

-- Dumped from database version 15.3
-- Dumped by pg_dump version 15.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category (
    id integer NOT NULL,
    is_delete boolean DEFAULT false NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.category OWNER TO postgres;

--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_id_seq OWNER TO postgres;

--
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.category_id_seq OWNED BY public.category.id;


--
-- Name: channel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.channel (
    user_id integer NOT NULL,
    notification_type_id integer NOT NULL
);


ALTER TABLE public.channel OWNER TO postgres;

--
-- Name: message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.message (
    id integer NOT NULL,
    is_delete boolean DEFAULT false NOT NULL,
    content character varying(255) NOT NULL,
    "Category_id" integer
);


ALTER TABLE public.message OWNER TO postgres;

--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.message_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.message_id_seq OWNER TO postgres;

--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.message_id_seq OWNED BY public.message.id;


--
-- Name: migration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migration (
    id integer NOT NULL,
    "timestamp" bigint NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.migration OWNER TO postgres;

--
-- Name: migration_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migration_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migration_id_seq OWNER TO postgres;

--
-- Name: migration_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migration_id_seq OWNED BY public.migration.id;


--
-- Name: notification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notification (
    id integer NOT NULL,
    is_delete boolean DEFAULT false NOT NULL,
    user_id integer NOT NULL,
    category_id integer NOT NULL,
    notification_type_id integer NOT NULL,
    log_date timestamp with time zone DEFAULT '2023-06-03 11:08:31.278-06'::timestamp with time zone NOT NULL,
    message_id integer NOT NULL
);


ALTER TABLE public.notification OWNER TO postgres;

--
-- Name: notification_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notification_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notification_id_seq OWNER TO postgres;

--
-- Name: notification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notification_id_seq OWNED BY public.notification.id;


--
-- Name: notification_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notification_type (
    id integer NOT NULL,
    is_delete boolean DEFAULT false NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.notification_type OWNER TO postgres;

--
-- Name: notification_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notification_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notification_type_id_seq OWNER TO postgres;

--
-- Name: notification_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notification_type_id_seq OWNED BY public.notification_type.id;


--
-- Name: subscription; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.subscription (
    user_id integer NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE public.subscription OWNER TO postgres;

--
-- Name: typeorm_metadata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.typeorm_metadata (
    type character varying NOT NULL,
    database character varying,
    schema character varying,
    "table" character varying,
    name character varying,
    value text
);


ALTER TABLE public.typeorm_metadata OWNER TO postgres;

--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    is_delete boolean DEFAULT false NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    telephone character varying(255)
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category ALTER COLUMN id SET DEFAULT nextval('public.category_id_seq'::regclass);


--
-- Name: message id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.message ALTER COLUMN id SET DEFAULT nextval('public.message_id_seq'::regclass);


--
-- Name: migration id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migration ALTER COLUMN id SET DEFAULT nextval('public.migration_id_seq'::regclass);


--
-- Name: notification id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification ALTER COLUMN id SET DEFAULT nextval('public.notification_id_seq'::regclass);


--
-- Name: notification_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification_type ALTER COLUMN id SET DEFAULT nextval('public.notification_type_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category (id, is_delete, name) FROM stdin;
1	f	Sports
2	f	Finance
3	f	Movies
\.


--
-- Data for Name: channel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.channel (user_id, notification_type_id) FROM stdin;
2	1
3	1
3	3
4	1
4	3
4	2
2	2
1	1
2	3
5	3
5	2
5	1
6	2
1	2
6	3
6	1
\.


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.message (id, is_delete, content, "Category_id") FROM stdin;
24	f	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s	1
26	f	It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.	1
25	f	Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old	2
27	f	New horror movie!! Gabriele Amorth (El Ganador del Oscar Russell Crowe) es un sacerdote mal hablado amante del whiskey y que saluda de una manera casi ofensiva a sus superiores.	\N
28	f	New horror movie!! Gabriele Amorth (El Ganador del Oscar Russell Crowe) es un sacerdote mal hablado amante del whiskey y que saluda de una manera casi ofensiva a sus superiores.	3
29	f	Banks are getting up!! With its $3.2 billion acquisition of Credit Suisse, UBS is poised to climb the ranks of global mega banks.	2
\.


--
-- Data for Name: migration; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migration (id, "timestamp", name) FROM stdin;
1	1685681824147	forthMigration1685681824147
2	1685681905793	forthMigration1685681905793
\.


--
-- Data for Name: notification; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notification (id, is_delete, user_id, category_id, notification_type_id, log_date, message_id) FROM stdin;
2	f	1	1	2	2023-06-02 17:26:06.183-06	24
3	f	2	1	1	2023-06-02 17:26:06.183-06	24
4	f	3	1	1	2023-06-02 17:26:06.183-06	24
5	f	3	1	2	2023-06-02 17:26:06.183-06	24
6	f	3	1	3	2023-06-02 17:26:06.183-06	24
7	f	1	1	2	2023-06-02 17:26:06.183-06	25
8	f	2	1	1	2023-06-02 17:26:06.183-06	25
9	f	3	1	1	2023-06-02 17:26:06.183-06	25
10	f	3	1	2	2023-06-02 17:26:06.183-06	25
11	f	3	1	3	2023-06-02 17:26:06.183-06	25
12	f	1	1	2	2023-06-02 17:52:58.994-06	26
13	f	2	1	1	2023-06-02 17:52:58.994-06	26
14	f	3	1	1	2023-06-02 17:52:58.994-06	26
15	f	3	1	2	2023-06-02 17:52:58.994-06	26
16	f	3	1	3	2023-06-02 17:52:58.994-06	26
17	f	2	3	1	2023-06-03 10:53:37.986-06	28
18	f	2	3	2	2023-06-03 10:53:37.986-06	28
19	f	2	3	3	2023-06-03 10:53:37.986-06	28
20	f	3	3	1	2023-06-03 10:53:37.986-06	28
21	f	3	3	3	2023-06-03 10:53:37.986-06	28
22	f	4	3	1	2023-06-03 10:53:37.986-06	28
23	f	4	3	2	2023-06-03 10:53:37.986-06	28
24	f	4	3	3	2023-06-03 10:53:37.986-06	28
25	f	5	3	1	2023-06-03 10:53:37.986-06	28
26	f	5	3	2	2023-06-03 10:53:37.986-06	28
27	f	5	3	3	2023-06-03 10:53:37.986-06	28
28	f	6	3	1	2023-06-03 10:53:37.986-06	28
29	f	6	3	2	2023-06-03 10:53:37.986-06	28
30	f	6	3	3	2023-06-03 10:53:37.986-06	28
31	f	1	2	1	2023-06-03 11:08:31.278-06	29
32	f	1	2	2	2023-06-03 11:08:31.278-06	29
33	f	2	2	1	2023-06-03 11:08:31.278-06	29
34	f	2	2	2	2023-06-03 11:08:31.278-06	29
35	f	2	2	3	2023-06-03 11:08:31.278-06	29
\.


--
-- Data for Name: notification_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notification_type (id, is_delete, name) FROM stdin;
1	f	SMS
2	f	E-Mail
3	f	Push Notification
\.


--
-- Data for Name: subscription; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.subscription (user_id, category_id) FROM stdin;
2	2
2	3
6	3
4	3
5	3
3	3
2	1
1	2
\.


--
-- Data for Name: typeorm_metadata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.typeorm_metadata (type, database, schema, "table", name, value) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (id, is_delete, first_name, last_name, email, telephone) FROM stdin;
1	f	first	user	first.user@gmail.com	9993-56-39-86
2	f	second	user	second.user@gmail.com	9993-56-39-87
3	f	third	user	third.user@gmail.com	9993-56-39-87
4	f	carlos	priego	priegoportillo@gmail.com	9993563986
5	f	Daniel	Priego	priegoportillo@hotmail.com	9993563986
6	f	juan	medina	juan.medina@gmail.com	9999101050
\.


--
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.category_id_seq', 3, true);


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.message_id_seq', 29, true);


--
-- Name: migration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migration_id_seq', 2, true);


--
-- Name: notification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notification_id_seq', 35, true);


--
-- Name: notification_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notification_type_id_seq', 3, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 6, true);


--
-- Name: migration PK_3043fc6b8af7c99b8b98830094f; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migration
    ADD CONSTRAINT "PK_3043fc6b8af7c99b8b98830094f" PRIMARY KEY (id);


--
-- Name: notification_type PK_3e0e1fa68c25d84f808ca11dbaa; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification_type
    ADD CONSTRAINT "PK_3e0e1fa68c25d84f808ca11dbaa" PRIMARY KEY (id);


--
-- Name: notification PK_588ae6435c5009e0fb80b5e9dde; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT "PK_588ae6435c5009e0fb80b5e9dde" PRIMARY KEY (id, user_id, category_id, notification_type_id, message_id);


--
-- Name: category PK_9c4e4a89e3674fc9f382d733f03; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT "PK_9c4e4a89e3674fc9f382d733f03" PRIMARY KEY (id);


--
-- Name: message PK_ba01f0a3e0123651915008bc578; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT "PK_ba01f0a3e0123651915008bc578" PRIMARY KEY (id);


--
-- Name: subscription PK_ba7a3519c2ea6fe5f4ad092b1e3; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscription
    ADD CONSTRAINT "PK_ba7a3519c2ea6fe5f4ad092b1e3" PRIMARY KEY (user_id, category_id);


--
-- Name: user PK_cace4a159ff9f2512dd42373760; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY (id);


--
-- Name: channel PK_e84c6044b863e0ae55aae792cd3; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.channel
    ADD CONSTRAINT "PK_e84c6044b863e0ae55aae792cd3" PRIMARY KEY (user_id, notification_type_id);


--
-- Name: notification FK_1c1ca42fa0d288c36d739c36e7b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT "FK_1c1ca42fa0d288c36d739c36e7b" FOREIGN KEY (category_id) REFERENCES public.category(id);


--
-- Name: notification FK_4c87b82a5cef04202495f67663c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT "FK_4c87b82a5cef04202495f67663c" FOREIGN KEY (notification_type_id) REFERENCES public.notification_type(id);


--
-- Name: message FK_68569be865a822487b51bcbe583; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT "FK_68569be865a822487b51bcbe583" FOREIGN KEY ("Category_id") REFERENCES public.category(id);


--
-- Name: channel FK_7e5d4007402f6c41e35003494f8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.channel
    ADD CONSTRAINT "FK_7e5d4007402f6c41e35003494f8" FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- Name: notification FK_928b7aa1754e08e1ed7052cb9d8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT "FK_928b7aa1754e08e1ed7052cb9d8" FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- Name: subscription FK_940d49a105d50bbd616be540013; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscription
    ADD CONSTRAINT "FK_940d49a105d50bbd616be540013" FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- Name: notification FK_bcacc62c929cc4881ec971b6791; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT "FK_bcacc62c929cc4881ec971b6791" FOREIGN KEY (message_id) REFERENCES public.message(id);


--
-- Name: channel FK_e22ec3f0ed51b953ab182c95430; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.channel
    ADD CONSTRAINT "FK_e22ec3f0ed51b953ab182c95430" FOREIGN KEY (notification_type_id) REFERENCES public.notification_type(id);


--
-- Name: subscription FK_e7ddc1213ce93c60338e6da254d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscription
    ADD CONSTRAINT "FK_e7ddc1213ce93c60338e6da254d" FOREIGN KEY (category_id) REFERENCES public.category(id);


--
-- PostgreSQL database dump complete
--

